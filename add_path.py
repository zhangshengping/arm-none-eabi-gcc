#!/usr/bin/python
import os
import sys
import stat

f = open("add_path.sh", "w")
s = "export PATH=$PATH:"
a,b = os.path.split(os.path.abspath("./gcc-arm-none-eabi-4_9-2015q3/bin/arm-none-eabi-gcc"))
s += a
f.write("#!/bin/bash\n")

f.write(s)
f.write("\n")
f.write("echo $PATH")
f.close()
os.chmod("./add_path.sh", stat.S_IRWXU|stat.S_IRGRP|stat.S_IROTH)
os.system("./add_path.sh")
os.system("rm ./add_path.sh")


